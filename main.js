const ipBtn = document.querySelector('.ip-btn');

ipBtn.addEventListener('click', async function(event){
    console.log(event);
    await fetch('https://api.ipify.org/?format=json').then(res => res.json()).then(data => {
        fetch(`http://ip-api.com/json/${data.ip}?fields=1634303`)
        .then(res => res.json())

        .then(info => {
            let continentInfo = document.getElementById('ip-value-continent');
            if (info.continent) {
                continentInfo.textContent = info.continent;
            }

            let countryInfo = document.getElementById('ip-value-country');
            if (info.country) {
                countryInfo.textContent = info.country;
            }

            let regionInfo = document.getElementById('ip-value-region');
            if (info.regionName) {
                regionInfo.textContent = info.regionName;
            }

            let cityInfo = document.getElementById('ip-value-city');
            if (info.city) {
                cityInfo.textContent = info.city;
            }

            let districtInfo = document.getElementById('ip-value-district');
            if (info.district) {
                districtInfo.textContent = info.district;
            }

            console.log(info, info.country, info.city, info.regionName, info.continent, info.district)
        })
        console.log(data)
})
})